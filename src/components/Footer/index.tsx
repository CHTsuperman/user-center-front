import { GithubOutlined } from '@ant-design/icons';
import { DefaultFooter } from '@ant-design/pro-layout';
import {MY_GITHUB} from "@/constants";

const Footer: React.FC = () => {
  const defaultMessage = '恒拓出品';
  const currentYear = new Date().getFullYear();

  return (
    <DefaultFooter
      copyright={`${currentYear} ${defaultMessage}`}
      links={[
        {
          key: 'leetcode',
          title: 'leetcode',
          href: 'https://leetcode.cn/u/cht-7/',
          blankTarget: true,
        },
        {
          key: 'planet',
          title: '知识星球',
          href: 'https://wx.zsxq.com/',
          blankTarget: true,
        },
        {
          key: 'github',
          title: <><GithubOutlined />恒拓 GitHub</>,
          href: MY_GITHUB,
          blankTarget: true,
        },
      ]}
    />
  );
};

export default Footer;
